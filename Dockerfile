FROM python:3.9-alpine

RUN apk --no-cache --update-cache add \
	gcc \
	gfortran \
	g++ \
	build-base \
	wget \
	freetype-dev \
	libpng-dev \
	openblas-dev

RUN ln -s /usr/include/locale.h /usr/include/xlocale.h

RUN pip install --upgrade pip
RUN pip install control==0.9.2
RUN pip install numpy==1.23.5
RUN pip install slycot
RUN pip install noise==1.2.2
RUN pip install gunicorn==18.0.0
RUN pip install Flask==1.0.2
RUN pip install flask-socketio==4.3.2
RUN pip install Jinja2==3.0.3
RUN pip install pdengine==0.0.4
